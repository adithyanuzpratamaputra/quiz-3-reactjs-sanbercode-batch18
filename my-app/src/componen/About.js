import React from 'react'

const About = () => {
    return (
        <div style="padding: 10px; border: 1px solid #ccc">
      <h1 style="text-align: center;">Data Peserta Sanbercode Bootcamp Reactjs</h1>
      <ol>
        <li><strong style="width: 100px;">Nama:</strong> Adithya Nuz Pratama Putra</li> 
        <li><strong style="width: 100px;">Email:</strong> adithyanuzpratamaputra@gmail.com</li> 
        <li><strong style="width: 100px;">Sistem Operasi yang digunakan:</strong> windows</li>
        <li><strong style="width: 100px;">Akun Gitlab:</strong> Adithya Nuz Pratama Putra</li> 
        <li><strong style="width: 100px;">Akun Telegram:</strong> akun telegram peserta</li> 
      </ol>
    </div>
    )
}

export default About