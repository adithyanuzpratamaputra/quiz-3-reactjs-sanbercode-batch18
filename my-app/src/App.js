import React from 'react';
import './App.css';
import Routes from './componen/Route';
import Nav from './componen/Nav'

function App() {
  return (
    <div className="App">
       <Routes>
        <Nav />
        <Routes />
      </Routes>
    </div>
  );
}

export default App;
